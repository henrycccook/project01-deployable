#!/usr/bin/python3
#Funtional testing and unit testing
#unit test every procedure
#Overall functional testing
#steps in the pipeline should be tested
#blog address https://ctrlaltdelor.wordpress.com/

#READ todo list
#use `./build` (including `mypy`)

import json

def is_StrictListOfStrictStrings(input_list: list) -> bool:
    return (
        is_nonEmptyList(input_list) and
        all([
            ( isinstance(x, str) and len(x) > 0 )
            for x in input_list
        ])
    )

def is_nonEmptyList (input_list: list) -> bool:
    return isinstance(input_list, list) and len(input_list) > 0

def is_simpleDict(simpleDict: dict) -> bool:
    return isinstance (simpleDict, dict) and len(simpleDict) == 1

def is_listOfStringsOrDictionaries():
    pass

def valueOfSimpleDict(simpleDict:dict):
    #pre
    if not is_simpleDict(simpleDict):
        raise TypeError
    #do
    Result = list(simpleDict.values())[0]
    return Result


################################################################
#Unit tests
import unittest
class Tests(unittest.TestCase):
    ExampleGoodInput= [ {"address":["StreetName","StreetHouseNumber",]}, "familyName", "firstName"]

    def assertAllTrue(self, it, *, msg=None):
        return self.assertNotIn(False, list(it), msg=msg)

    def test_is_StrictListOfStrictStrings(self):
        self.assertFalse( is_StrictListOfStrictStrings(1) )
        self.assertFalse( is_StrictListOfStrictStrings([]) )
        self.assertTrue ( is_StrictListOfStrictStrings(["a string"]) )
        self.assertFalse( is_StrictListOfStrictStrings([1]))
        self.assertFalse( is_StrictListOfStrictStrings([""]) )
        self.assertTrue(  is_StrictListOfStrictStrings(["a", "b"]) )
        self.assertFalse( is_StrictListOfStrictStrings(["a", ""]) )
        self.assertFalse( is_StrictListOfStrictStrings(["a", 1]) )
        self.assertFalse( is_StrictListOfStrictStrings([1, "a"]) )
        self.assertFalse( is_StrictListOfStrictStrings(["", 1]) )
        self.assertTrue(  is_StrictListOfStrictStrings(["StreetName", "StreetHouseNumber", "numberOfArdvarks", "cat"]) )

    def test_is_nonEmptyList(self):
        self.assertFalse(is_nonEmptyList(1), msg="Not a list")
        self.assertTrue( is_nonEmptyList(["a"]), msg="shortest list")
        self.assertFalse(is_nonEmptyList([]))
        self.assertTrue (is_nonEmptyList([ "a", "b"]), msg="longer list")

    def test_isSimpleDict(self):
        self.assertTrue(  is_simpleDict({"a":"b"}) )
        self.assertFalse( is_simpleDict("a") )
        self.assertFalse( is_simpleDict({}) )
        self.assertFalse( is_simpleDict({"a":"b", "c":"d"}) )


    def test_is_listOfStringsOrDictionaries(self): #:todo: we are here
        is_listOfStringsOrDictionaries("a")
        
    def test_valueOfSimpleDict(self):
        self.assertEqual( valueOfSimpleDict( {"key": "value" }), "value", msg="Simple dict, returns its value")
        with self.assertRaises(TypeError, msg="simple dict must be simple: one entry"):
            valueOfSimpleDict({"key": "value", "key2": "value2" })
        with self.assertRaises(TypeError, msg="Not a dictionary"):
            valueOfSimpleDict("n")

    def test_a(self):
        template=self.ExampleGoodInput
        self.assertTrue(is_nonEmptyList(template))
        self.assertAllTrue( #:todo: has pyunit got one of these? We did a check and found nothing.
            [
                isinstance (x, (str, dict)) for x in
                template
            ]
        )

        iter_of_dictionaries= filter(lambda x: isinstance(x, dict), template)
        self.assertAllTrue(
            [
                (
                    self.assertTrue( is_StrictListOfStrictStrings( valueOfSimpleDict(d) ) )
                )
                for d in iter_of_dictionaries
            ],
                msg="A dictionary in the input has more that one key, or a value is not a list of strings"
        )

        # for each dictionary check all keys are strings: no explicit test needed (done by json load)
        # :todo:next: check that  values are lists DONE
        # :todo:next: check that the lists that are values in the dictionaries only contain strings
        # :todo:thought: separate tests out into different lines/ tests so that its easy to see which failed (can you name tests or have them print an error message?
        # :todo: review self
        # :todo: tidy comments (including the comment that replaces code: make it a do-nothing function)

        # What else about this file do we need to test?
        # - is json  (done)
        # - is a list
        # - not empty list
        # - items in list are string, or object of ...

if __name__ == '__main__':
    unittest.main()
